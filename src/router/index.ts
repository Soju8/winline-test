import { createRouter, createWebHistory } from 'vue-router'
import StartCardsOnTimelinePage from '@/pages/StartCardsOnTimeline.vue'
import CardsOnTimelinePage from '@/pages/CardsOnTimeline.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
        path: '/',
        redirect: '/way_to_bonus'
    },
    {
      path: '/way_to_bonus',
      name: 'start_cards_on_timeline',
      component: StartCardsOnTimelinePage
    },
    {
      path: '/way_to_bonus',
      name: 'cards_on_timeline',
      component: CardsOnTimelinePage
    }
  ]
})

export default router
